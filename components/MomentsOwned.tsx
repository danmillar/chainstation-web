import { IPFS_GATEWAY, MOMENTS_ADDRESS, switchToChain, useFetchStatus } from 'lib/util'
import { useEffect, useState } from 'react'
import { Address, parseAbi } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import { multicall, readContract } from 'wagmi/actions'
import { Moment } from 'lib/types'
const moments: Moment[] = require('lib/moments_meta.json')

const MOMENTS = {
  address: MOMENTS_ADDRESS as `0x${string}`,
  abi: parseAbi([
    'function balanceOf(address owner) external view returns (uint256)',
    'function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256)',
    'function getMomentId(uint256 tokenId) external view returns (uint256)',
  ]),
} as const

interface MomentRef {
  tokenId: number
  moment: Moment
}

interface Props {
  address: Address
}

const ZWS = '\u200B'

export const MomentsOwned = ({ address }: Props) => {
  const [status, setStatus] = useFetchStatus()
  const [ownedMoments, setMoments] = useState<MomentRef[]>([])

  useEffect(() => {
    let ignore = false

    async function doWork() {
      setStatus('pending')
      if (!(await switchToChain(1))) {
        console.error('Failed to switch networks')
        setStatus('error')
        return
      }

      // Start with fetching how many Moments this address owns
      const count = await readContract({
        ...MOMENTS,
        functionName: 'balanceOf',
        args: [address],
      })
      if (ignore) return
      if (count == 0n) {
        setMoments([])
        setStatus('done')
        return
      }

      // Iterate up to that moment count number to fetch which Moment ID it is
      let multicalls = []
      for (let i = 0n; i < count; i++) {
        multicalls.push({
          ...MOMENTS,
          functionName: 'tokenOfOwnerByIndex',
          args: [address, i],
        } as const)
      }
      const ownedTokenIds = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      // For each token ID, find out which Moment batch it's from
      multicalls = []
      for (let tokenId of ownedTokenIds) {
        multicalls.push({
          ...MOMENTS,
          functionName: 'getMomentId',
          args: [tokenId],
        } as const)
      }
      const ownedMomentIds = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      const ownedMoments: MomentRef[] = []
      for (let i = 0; i < ownedTokenIds.length; i++) {
        const tokenId = ownedTokenIds[i]
        const momentId = ownedMomentIds[i]
        ownedMoments.push({
          tokenId: Number(tokenId),
          moment: moments[Number(momentId)],
        })
      }
      setMoments(ownedMoments)
      setStatus('done')
    }
    doWork()

    return () => {
      ignore = true
    }
  }, [address, setStatus])

  if (status == 'start' || status == 'pending') {
    return (
      <>
        <h2>MoonCat{ZWS}Moments</h2>
        <LoadingIndicator />
      </>
    )
  }
  if (ownedMoments.length == 0) return null

  return (
    <>
      <h2>MoonCat{ZWS}Moments</h2>
      <div id="item-grid" style={{ margin: '1em 0' }}>
        {ownedMoments.map((m) => {
          const imgSrc = m.moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
          return (
            <a key={m.tokenId} className="item-thumb" href={`/moments/${m.moment.momentId}`}>
              <div className="thumb-img" style={{ backgroundImage: `url(${imgSrc})` }} />
              <p>#{m.tokenId}</p>
              <p>{m.moment.meta.name}</p>
            </a>
          )
        })}
      </div>
    </>
  )
}
