import React, { CSSProperties } from 'react'
import Image from 'next/image'

interface Props {
  message?: String
  style?: CSSProperties
}

const LoadingIndicator = ({ message = 'Fetching fresh data from the blockchain...', style }: Props) => {
  let wrapperStyle = Object.assign({ fontSize: '0.8em' }, style)
  return (
    <div style={wrapperStyle}>
      <span
        style={{
          display: 'inline-block',
          position: 'relative',
          width: '1.5em',
          height: '1.5em',
          verticalAlign: '-0.4em',
          marginRight: '0.5em',
        }}
      >
        <Image src="/img/loading.svg" alt="" layout="fill" />
      </span>
      {message}
    </div>
  )
}
export default LoadingIndicator
