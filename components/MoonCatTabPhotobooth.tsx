import React, { useEffect, useState } from 'react'
import { TabProps } from '../pages/mooncats/[id]'
import { API_SERVER_ROOT } from 'lib/util'
import { SelectField } from './FormFields'
import AccessoryPicker from './AccessoryPicker'
import useMoonCatAccessories, { OwnedAccessory } from 'lib/useMoonCatAccessories'
import LoadingIndicator from './LoadingIndicator'

interface SizeChoice {
  padding?: number
  label: string
}
interface SizeScaleChoice extends SizeChoice {
  scale: number
}
interface SizeBoundsChoice extends SizeChoice {
  width: number
  height: number
}
function isScaleChoice(choice: SizeChoice | SizeScaleChoice | SizeBoundsChoice): choice is SizeScaleChoice {
  return typeof (choice as SizeScaleChoice).scale != 'undefined'
}
function isBoundsChoice(choice: SizeChoice | SizeScaleChoice | SizeBoundsChoice): choice is SizeBoundsChoice {
  return (
    typeof (choice as SizeBoundsChoice).width != 'undefined' ||
    typeof (choice as SizeBoundsChoice).height != 'undefined'
  )
}

const sizeChoices: Record<string, SizeChoice | SizeScaleChoice | SizeBoundsChoice> = {
  default: { label: 'Default' },
  tiny: { scale: 1, padding: 0, label: 'Tiny' },
  small: { scale: 2, padding: 0, label: 'Small' },
  medium: { scale: 5, padding: 0, label: 'Medium' },
  large: { scale: 8, padding: 0, label: 'Large' },
  twitter: { width: 400, height: 400, label: 'Twitter' },
  discord: { width: 128, height: 128, label: 'Discord' },
  reddit: { width: 256, height: 256, label: 'Reddit' },
  youtube: { width: 800, height: 800, label: 'YouTube' },
}
let sizeOptions: Record<string, string> = {}
Object.keys(sizeChoices).forEach((s) => {
  sizeOptions[s] = sizeChoices[s].label
})

async function getImageBounds(url: string): Promise<{ height: number; width: number }> {
  let rs = await fetch(url)
  if (!rs.ok) {
    return {
      height: 100,
      width: 100,
    }
  }
  if (!rs.headers.has('X-Image-Height')) {
    return {
      height: 100,
      width: 100,
    }
  }
  return {
    height: parseInt(rs.headers.get('X-Image-Height') as string),
    width: parseInt(rs.headers.get('X-Image-Width') as string),
  }
}

const MoonCatTabPhotobooth = ({ moonCat, details }: TabProps) => {
  const { status, ownedAccessories } = useMoonCatAccessories(moonCat.rescueOrder)
  const [sizeChoice, setSizeChoice] = useState('default')
  const [moonCatChoice, setMoonCatChoice] = useState('full')

  const [chosenAccessories, setChosenAccessories] = useState<OwnedAccessory[]>([])
  useEffect(() => {
    setChosenAccessories(
      ownedAccessories.sort((a, b) => {
        if (a.zIndex != b.zIndex) {
          return b.zIndex - a.zIndex
        }
        return a.name.localeCompare(b.name)
      })
    )
  }, [ownedAccessories])
  const [imageURI, setImageURI] = useState<string>(API_SERVER_ROOT + '/image/' + moonCat.rescueOrder)

  // If the user picks a size option that is a bounds-style config, that requires async calls, so this
  // needs to be an effect, which then re-triggers whenever one of the form fields change their value.
  useEffect(() => {
    let ignore = false
    let params = ['costumes=true']
    if (moonCatChoice == 'head') params.push('headOnly')
    const sizeChoiceData = sizeChoices[sizeChoice]

    const urlBase = API_SERVER_ROOT + '/image/' + moonCat.rescueOrder

    // Add in chosen Accessories
    const chosenForegroundAccessories = chosenAccessories.filter((a) => a.zIndex > 0 && !a.isBackground).reverse()
    const chosenBackgroundAccessories = chosenAccessories.filter((a) => a.zIndex > 0 && a.isBackground)
    params.push(
      'acc=' +
        chosenBackgroundAccessories
          .concat(chosenForegroundAccessories)
          .map((a) => {
            if (a.availablePalettes > 1) return `${a.accessoryId}:${a.paletteIndex}`
            return a.accessoryId
          })
          .join(',')
    )

    if (isScaleChoice(sizeChoiceData)) {
      // Scale value is specified; just use it
      params.push('scale=' + sizeChoiceData.scale)
      if (typeof sizeChoiceData.padding != 'undefined') params.push('padding=' + sizeChoiceData.padding)
      setImageURI(urlBase + '?' + params.join('&'))
      return
    } else if (isBoundsChoice(sizeChoiceData)) {
      // Calculate scale based on target height/width
      getImageBounds(urlBase + '?scale=1&padding=0&' + params.join('&')).then((baseSize) => {
        if (ignore) return
        let widthFactor = sizeChoiceData.width / baseSize.width
        let heightFactor = sizeChoiceData.height / baseSize.height
        console.log(sizeChoiceData, baseSize, widthFactor, heightFactor)
        let scale, padding
        if (widthFactor < heightFactor) {
          scale = Math.floor(widthFactor)
          padding = Math.floor((sizeChoiceData.height - baseSize.height * scale) / 2)
        } else {
          scale = Math.floor(heightFactor)
          padding = Math.floor((sizeChoiceData.width - baseSize.width * scale) / 2)
        }
        if (scale < 1) scale = 1
        if (padding < 1) padding = 0
        params.push('scale=' + scale)
        params.push('padding=' + padding)
        setImageURI(urlBase + '?' + params.join('&'))
        return
      })
    } else {
      // Just using defaults
      if (typeof sizeChoiceData.padding != 'undefined') params.push('padding=' + sizeChoiceData.padding)
      setImageURI(urlBase + '?' + params.join('&'))
      return
    }

    return () => {
      ignore = true
    }
  }, [moonCat.rescueOrder, sizeChoice, moonCatChoice, chosenAccessories])

  const labelStyle = { width: '200px' }

  return (
    <section>
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <div id="form-controls" style={{ flex: '1 1 40%', padding: '2rem 0.5rem 0 0' }}>
          <SelectField
            meta={{
              name: 'size',
              type: 'select',
              label: 'Size',
              options: sizeOptions,
            }}
            labelStyle={labelStyle}
            currentValue={sizeChoice}
            onChange={(e) => setSizeChoice(e.target.value)}
          />
          <SelectField
            meta={{
              name: 'mooncat',
              type: 'select',
              label: 'MoonCat appearance',
              options: {
                full: 'Full body',
                head: 'Head only',
              },
            }}
            labelStyle={labelStyle}
            currentValue={moonCatChoice}
            onChange={(e) => setMoonCatChoice(e.target.value)}
          />

          <div id="accessories-configuration">
            <h2>Accessories</h2>
            {status != 'done' && <LoadingIndicator />}
            {status == 'done' && <AccessoryPicker accessories={chosenAccessories} onChange={setChosenAccessories} />}
          </div>
        </div>
        <div style={{ flex: '1 1 40%', padding: '0 0.5rem', fontSize: '0.8rem' }}>
          <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '2em' }}>
            <picture>
              <img
                alt={'MoonCat ' + moonCat.rescueOrder}
                src={imageURI}
                style={{ maxWidth: 640, border: 'solid 3px #888', boxShadow: '8px 8px 0 #888' }}
              />
            </picture>
          </div>
          <p>
            Right-click and select <strong>&ldquo;Save image as...&rdquo;</strong> to download.
          </p>
          <p>
            To reference this image directly, use <code>{imageURI}</code>
          </p>
        </div>
      </div>
    </section>
  )
}

export default MoonCatTabPhotobooth
