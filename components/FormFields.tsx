import { FieldProps, SelectFieldMeta, TextFieldMeta, isSelectFieldMeta } from 'lib/types'

/**
 * Render an individual form field, using a HTML SELECT element as the control
 */
export const SelectField = ({
  meta,
  labelStyle,
  currentValue,
  onChange,
}: FieldProps<SelectFieldMeta, HTMLSelectElement>) => {
  const defaultValue = typeof meta.defaultLabel == 'undefined' ? null : <option value="">{meta.defaultLabel}</option>
  return (
    <label className="field-row">
      <div className="field-label" style={labelStyle}>
        {meta.label}
      </div>
      <div className="field-control">
        <select name={meta.name} value={currentValue} onChange={onChange}>
          {defaultValue}
          {Object.keys(meta.options).map((optionValue) => {
            return (
              <option key={optionValue} value={optionValue}>
                {meta.options[optionValue]}
              </option>
            )
          })}
        </select>
      </div>
    </label>
  )
}

export const TextField = ({
  meta,
  labelStyle,
  currentValue,
  onChange,
}: FieldProps<TextFieldMeta, HTMLInputElement>) => {
  if (isSelectFieldMeta(meta)) return <>Bad TextField Meta</>
  return (
    <label className="field-row">
      <div className="field-label" style={labelStyle}>
        {meta.label}
      </div>
      <div className="field-control">
        <input type="text" name={meta.name} value={currentValue} onChange={onChange} />
      </div>
    </label>
  )
}
