import React, { CSSProperties, MouseEventHandler } from 'react'
import Icon from './Icon'

interface Props {
  name: string
  onClick: MouseEventHandler<HTMLButtonElement>
  title?: string
  style?: CSSProperties
}

const IconButton = ({ name, onClick, title, style }: Props) => {
  return (
    <button className="icon-button" onClick={onClick} title={title} style={style}>
      <Icon name={name} />
    </button>
  )
}

export default IconButton
