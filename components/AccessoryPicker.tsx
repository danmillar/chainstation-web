import { OwnedAccessory } from 'lib/useMoonCatAccessories'
import React from 'react'
import IconButton from './IconButton'

interface Props {
  accessories: OwnedAccessory[]
  onChange: Function
}

const AccessoryGroup = ({
  accessories,
  label,
  onChange,
}: {
  accessories: OwnedAccessory[]
  label: string
  onChange: (list: OwnedAccessory[]) => any
}) => {
  if (accessories.length == 0) return <p>No {label} accessories owned</p>

  /**
   * A button on an individual Accessory row was clicked.
   * Modify the `accessories` array according to that action type, and return the transformed
   * `accessories` array to the parent component that included this one.
   */
  function handleClick(index: number, action: 'up' | 'down' | 'show' | 'hide'): void {
    switch (action) {
      case 'up':
        if (index == 0) break
        ;[accessories[index], accessories[index - 1]] = [accessories[index - 1], accessories[index]]
        break
      case 'down':
        if (index >= accessories.length - 1) break
        ;[accessories[index + 1], accessories[index]] = [accessories[index], accessories[index + 1]]
        break
      case 'show':
        accessories[index].zIndex = 1
        break
      case 'hide':
        accessories[index].zIndex = 0
        break
    }
    onChange(accessories)
  }

  function handlePaletteChange(index: number, newPalette: number) {
    accessories[index].paletteIndex = newPalette
    onChange(accessories)
  }

  const buttonStyle = {
    padding: '0.2rem',
  }

  return (
    <ul>
      {accessories.map((a, i) => {
        let buttons: React.ReactNode[] = []
        if (a.availablePalettes > 1) {
          buttons.push(
            <select
              key="palette"
              value={a.paletteIndex}
              onChange={(e) => handlePaletteChange(i, parseInt(e.target.value))}
            >
              {[...Array(a.availablePalettes)].map((m, i) => (
                <option key={i}>{i}</option>
              ))}
            </select>
          )
        }
        buttons.push(
          <IconButton
            key="up"
            title="Move Up"
            style={buttonStyle}
            onClick={(e) => handleClick(i, 'up')}
            name="arrow-up-light"
          />
        )
        buttons.push(
          <IconButton
            key="down"
            title="Move Down"
            style={buttonStyle}
            onClick={(e) => handleClick(i, 'down')}
            name="arrow-down-light"
          />
        )
        if (a.zIndex > 0) {
          buttons.push(
            <IconButton
              key="hide"
              title="Hide"
              style={buttonStyle}
              onClick={(e) => handleClick(i, 'hide')}
              name="eye"
            />
          )
        }
        if (a.zIndex == 0) {
          buttons.push(
            <IconButton
              key="show"
              title="Show"
              style={buttonStyle}
              onClick={(e) => handleClick(i, 'show')}
              name="eye-blocked"
            />
          )
        }

        const classNames = a.zIndex == 0 ? 'hidden' : ''

        return (
          <li key={Number(a.accessoryId)} className={classNames}>
            <span className="label">{a.name}</span>
            <span className="buttons">{buttons}</span>
          </li>
        )
      })}
    </ul>
  )
}

const AccessoryPicker = ({ accessories, onChange }: Props) => {
  const foregroundAccessories = accessories.filter((a) => !a.isBackground)
  const backgroundAccessories = accessories.filter((a) => a.isBackground)

  return (
    <section className="accessory-picker">
      <h3>Foreground</h3>
      <AccessoryGroup
        accessories={foregroundAccessories}
        label="foreground"
        onChange={(newList) => onChange(newList.concat(...backgroundAccessories))}
      />
      <h3>Background</h3>
      <AccessoryGroup
        accessories={backgroundAccessories}
        label="background"
        onChange={(newList) => onChange(foregroundAccessories.concat(...newList))}
      />
    </section>
  )
}

export default AccessoryPicker
