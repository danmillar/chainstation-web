import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'
import { useIsMounted } from 'lib/util'
import { useAccount } from 'wagmi'
import { useWeb3Modal } from '@web3modal/react'

const ZWS = '\u200B'

const Navigation = () => {
  const [menuOpen, setMenuOpen] = useState<boolean>(false)
  const isMounted = useIsMounted()
  const { address, isConnected } = useAccount()
  const { open } = useWeb3Modal()
  const router = useRouter()

  let connectView: React.ReactNode
  if (isMounted) {
    if (isConnected && !!address) {
      connectView = <EthereumAddress address={address} style={{ background: 'transparent' }} />
    } else {
      connectView = <button onClick={(e) => open()}>Connect</button>
    }
  }

  // When user navigates to a new page, close the menu display
  useEffect(() => {
    setMenuOpen(false)
  }, [router.asPath])

  const appTitle = 'MoonCat' + ZWS + 'Rescue'
  const menuClass = menuOpen ? 'menu-open' : 'menu-closed'
  return (
    <nav id="sidebar">
      <div id="mobile-menu-button" onClick={() => setMenuOpen(!menuOpen)} style={{ cursor: 'pointer' }}>
        <Icon name="menu" />
      </div>
      <div id="logo">
        <Link href="/">{appTitle}</Link>
      </div>
      <div id="menu" className={menuClass} style={{ flex: '1 10 auto', overflowY: 'auto' }}>
        <h2>Object Spectrometer</h2>
        <ul>
          <li>
            <Link href="/mooncats">MoonCats</Link>
          </li>
          <li>
            <Link href="/accessories">Accessories</Link>
          </li>
          <li>
            <Link href="/lootprints">lootprints</Link>
          </li>
          <li>
            <Link href="/moments">Moments</Link>
          </li>
        </ul>
        <h2>Tools</h2>
        <ul>
          <li>
            <Link href="/owners">Owner Profiles</Link>
          </li>
          <li>
            <Link href="/acclimator">Acclimator</Link>
          </li>
          <li>
            <Link href="/boutique">Boutique</Link>
          </li>
          <li>
            <Link href="/mcns">MoonCatNameService</Link>
          </li>
        </ul>
        <h2>Info</h2>
        <ul>
          <li>
            <Link href="/about">About</Link>
          </li>
          <li>
            <Link href="/team/contribute">Contribute</Link>
          </li>
          <li>
            <Link href="/profile">My Profile</Link>
          </li>
        </ul>
      </div>
      <div id="wallet-connection">{connectView}</div>
    </nav>
  )
}
export default Navigation
