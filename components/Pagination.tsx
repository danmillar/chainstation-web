import React from 'react'
import Icon from './Icon'

interface Props {
  currentPage: number
  maxPage: number
  setCurrentPage: (newValue: number) => void
}

/**
 * Shows a pagination component to go forward and backward.
 *
 * `currentPage` and `maxPage` are zero-based, but will be displayed as one-based
 */
const Pagination = ({ currentPage, maxPage, setCurrentPage }: Props) => {
  if (maxPage == 0) return null

  function back() {
    if (currentPage <= 0) return
    setCurrentPage(currentPage - 1)
  }
  function next() {
    if (currentPage >= maxPage) return
    setCurrentPage(currentPage + 1)
  }
  function makeSkipLink(skipSize: number, separator: string = '..'): React.ReactNode {
    let targetPage = currentPage + skipSize
    if (skipSize < 0) {
      return (
        <>
          <span
            className="pagination-skip-left alt-link"
            onClick={() => {
              setCurrentPage(targetPage)
            }}
          >
            {targetPage + 1}
          </span>
          <span>{separator}</span>
        </>
      )
    } else {
      return (
        <>
          <span>{separator}</span>
          <span
            className="pagination-skip-right alt-link"
            onClick={() => {
              setCurrentPage(targetPage)
            }}
          >
            {targetPage + 1}
          </span>
        </>
      )
    }
  }

  const backClasses = currentPage > 0 ? 'pagination-left alt-link' : 'pagination-left alt-link disabled'
  const forwardClasses = currentPage < maxPage ? 'pagination-right alt-link' : 'pagination-right alt-link disabled'

  const backBeginning = currentPage >= 15 ? makeSkipLink(currentPage * -1, '...') : null
  const backTen = currentPage >= 10 ? makeSkipLink(-10) : null
  const backFive = currentPage >= 5 ? makeSkipLink(-5) : null
  const forwardFive = maxPage - currentPage >= 5 ? makeSkipLink(5) : null
  const forwardTen = maxPage - currentPage >= 10 ? makeSkipLink(10) : null
  const forwardEnd = maxPage - currentPage >= 15 ? makeSkipLink(maxPage - currentPage, '...') : null
  return (
    <div className="pagination text-scrim">
      <div className={backClasses} onClick={back}>
        <Icon name="arrow-left" />
      </div>
      <div className="pagination-center">
        {backBeginning}
        {backTen}
        {backFive}
        <span>
          <strong>{currentPage + 1}</strong>
        </span>
        {forwardFive}
        {forwardTen}
        {forwardEnd}
      </div>
      <div className={forwardClasses} onClick={next}>
        <Icon name="arrow-right" />
      </div>
    </div>
  )
}
export default Pagination
