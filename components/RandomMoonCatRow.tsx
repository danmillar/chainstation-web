import React from 'react'
import RandomMoonCat from './RandomMoonCat'

const RandomMoonCatRow = (): JSX.Element => {
  return (
    <div className="mooncat-row">
      <div>
        <RandomMoonCat />
      </div>
      <div>
        <RandomMoonCat />
      </div>
      <div>
        <RandomMoonCat />
      </div>
      <div className="only-wide">
        <RandomMoonCat />
      </div>
    </div>
  )
}
export default RandomMoonCatRow
