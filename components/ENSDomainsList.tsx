import React from 'react'
import { Address, useAccount, useContractRead, useContractReads } from 'wagmi'
import { waitForTransaction, writeContract } from 'wagmi/actions'
import { customEvent } from 'lib/analytics'
import { switchToChain, useIsMounted } from 'lib/util'
import useTxStatus from 'lib/useTxStatus'
import Icon from 'components/Icon'
import LoadingIndicator from 'components/LoadingIndicator'
import { BaseError, formatEther, keccak256, parseAbi, toBytes } from 'viem'

/**
 * Calculate an ENS-compatible "labelhash" for a string
 *
 * This is different than the "namehash" algorithm
 */
function toLabelhash(label: string) {
  return keccak256(toBytes(label))
}

/**
 * Turn a Javascript Date object into a human-friendly "YYYY.MM.DD" string
 */
function dateLabel(date: Date) {
  const paddedMonth = ('00' + (date.getMonth() + 1)).slice(-2)
  const paddedDay = ('00' + date.getDate()).slice(-2)
  return `${date.getFullYear()}.${paddedMonth}.${paddedDay}`
}

/**
 * Render a bulleted list item describing a single ENS domain and its expiration date
 */
const ENSDomainLine = ({ ensName, expiresTimestamp }: { ensName: string; expiresTimestamp: number | undefined }) => {
  if (!expiresTimestamp) {
    // Fall back to just providing a link to the name
    return (
      <li>
        <a href={`https://app.ens.domains/${ensName}`}>
          <code>{ensName}</code>
        </a>
      </li>
    )
  }

  const expiresDate = new Date(expiresTimestamp)
  const expiresDays = Math.floor((expiresTimestamp - Date.now()) / 1000 / 60 / 60 / 24)
  const expiresYears = Math.floor(expiresDays / 365)

  const expiresLabel = expiresDays > 400 ? expiresYears + ' years' : expiresDays + ' days'
  if (expiresDays < 60) {
    // Address is close to expiring
    return (
      <li>
        <a href={`https://app.ens.domains/${ensName}`}>
          <code>{ensName}</code>
        </a>
        :{' '}
        <span style={{ color: 'hsl(15, 68%, 50%)' }}>
          <Icon name="warning" style={{ marginRight: '0.2em', verticalAlign: '-0.15em' }} />
          <span title={expiresDate.toISOString()}>
            {expiresLabel} ({dateLabel(expiresDate)})
          </span>
        </span>
      </li>
    )
  }

  return (
    <li>
      <a href={`https://app.ens.domains/${ensName}`}>
        <code>{ensName}</code>
      </a>
      :{' '}
      <span title={expiresDate.toISOString()}>
        {expiresLabel} ({dateLabel(expiresDate)})
      </span>
    </li>
  )
}

/**
 * Render a button that extends a single ENS domain by three years
 */
const ExtendButton = ({ ensName, onExtend }: { ensName: string; onExtend: Function | undefined }) => {
  const { status: extendStatus, viewMessage: extendMessage, setStatus: setExtendStatus } = useTxStatus()
  const ensLabel = ensName.split('.')[0]
  const renewalTime = 94608000n

  const config = {
    address: '0x253553366Da8546fC250F225fe3d25d0C782303b' as `0x${string}`,
    abi: parseAbi([
      'function renew(string name, uint256 duration) external payable',
      'function rentPrice(string name, uint256 duration) external view returns (( uint256 base, uint256 premium ))',
    ]),
  }

  const { data, error, status } = useContractRead({
    ...config,
    functionName: 'rentPrice',
    args: [ensLabel, renewalTime],
  })
  if (status == 'error') {
    console.error(error)
  }
  if (typeof data === 'undefined') {
    return <LoadingIndicator />
  }

  let totalCost = data.base + data.premium
  totalCost = totalCost + totalCost / 50n // 102% of price estimate; in case price fluctuates.

  const doExtend = async () => {
    setExtendStatus('building')
    if (!(await switchToChain(1))) {
      setExtendStatus('error', 'Wrong network chain')
      return
    }

    let renewPromise = writeContract({
      ...config,
      functionName: 'renew',
      args: [ensLabel, renewalTime],
      value: totalCost,
    })
    setExtendStatus('authorizing')
    try {
      let { hash } = await renewPromise
      setExtendStatus(
        'done',
        <p>
          <strong>Thank you!</strong>
        </p>
      )
      await waitForTransaction({ hash })
      if (typeof onExtend == 'function') {
        onExtend()
      }
      customEvent('ens_extend', {
        'ens_domain': ensName,
      })
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setExtendStatus('error', err.shortMessage)
      } else {
        console.log('Transaction unknown error', err)
        setExtendStatus('error', 'Transaction error')
      }
    }
  }

  return (
    <>
      <p>
        To assist the MoonCatRescue project in this way, either click one of the above ENS names (which will take you to
        the ENS web UI) to do a configurable extension action, or click the button below to initiate a transaction to
        renew the next-expiring ENS domain from this list (3 year extension for{' '}
        <Icon name="ethereum" style={{ marginRight: '0.1em', verticalAlign: '-0.15em' }} />
        {formatEther(totalCost).slice(0, 7)} ETH).
      </p>
      {extendStatus == 'start' && (
        <p>
          <button onClick={doExtend}>Extend {ensName}</button>
        </p>
      )}
      {extendMessage}
    </>
  )
}

const ensNames = ['mooncatcommunity.eth', 'mooncatrescue.eth', 'ismymooncat.eth', 'mymooncat.eth', 'gravball.eth']

/**
 * Render a list of ENS domains, the dates they expire, and a call-to-action to renew one of them
 */
const ENSDomainsList = () => {
  const isMounted = useIsMounted()
  const { isConnected } = useAccount()

  // Fetch all the expiration timestamps for all the ENS domains in the list
  let contractCalls = ensNames.map((ensName) => {
    return {
      address: '0x57f1887a8BF19b14fC0dF6Fd9B2acc9Af147eA85' as Address,
      abi: parseAbi(['function nameExpires(uint256 id) external view returns (uint256)']),
      functionName: 'nameExpires',
      args: [toLabelhash(ensName.split('.')[0])],
    }
  })
  const { data, error, status, refetch } = useContractReads({
    contracts: contractCalls,
  })

  // Parse the expiration timestamps and track which one is closest to expiring
  let expirations: number[] = []
  let nextExpiring = null
  if (status == 'error') {
    console.error(error)
  } else if (isMounted && status == 'success' && typeof data != 'undefined') {
    expirations = data.map((d) => {
      return Number(d.result) * 1000
    })
    nextExpiring = ensNames[expirations.indexOf(Math.min.apply(Math, expirations))]
  }

  let cta
  if (!isConnected || nextExpiring == null) {
    // If the user doesn't have a connected wallet, or we don't know which domain is expiring next, the call-to-action is to direct them to the ENS app
    cta = (
      <p>
        To assist the MoonCatRescue project in this way, click one of the above ENS names (which will take you to the
        ENS web UI) and click the &ldquo;Extend&rdquo; button to renew that domain&rsquo;s time.
      </p>
    )
  } else {
    // If the user has a connected wallet, and we know which ENS domain is expiring next, the call-to-action is to do an extension.
    cta = <ExtendButton ensName={nextExpiring} onExtend={refetch} />
  }

  return (
    <>
      <ul>
        {ensNames.map((ensName, index) => (
          <ENSDomainLine key={ensName} ensName={ensName} expiresTimestamp={expirations[index]} />
        ))}
      </ul>
      {cta}
    </>
  )
}
export default ENSDomainsList
