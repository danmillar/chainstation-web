import React, { useContext } from 'react'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import { SIWE_LOGIN_STATEMENT, useFetchStatus, AppVisitorContext, doUserCheck } from 'lib/util'
import { SiweMessage } from 'siwe'
import { useNetwork, useSignMessage, Address } from 'wagmi'
import Link from 'next/link'

interface Props {
  address: Address
  onSignedIn?: () => void
  onSignedOut?: () => void
}
const SignIn = ({ address, onSignedIn, onSignedOut }: Props) => {
  const { chain } = useNetwork()
  const { signMessageAsync } = useSignMessage()
  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const {
    state: {
      verifiedAddresses: { status, value: verifiedAddresses },
    },
    dispatch,
  } = useContext(AppVisitorContext)

  // User clicked the sign-in button
  const signIn: React.MouseEventHandler<any> = async function signIn(e) {
    e.preventDefault()
    if (!dispatch) return
    const chainId = chain?.id
    if (!chainId) return

    setFetchStatus('pending')
    const nonceRes = await fetch('/api/nonce')
    const nonce = await nonceRes.text()

    const expires = new Date()
    expires.setTime(expires.getTime() + 24 * 60 * 60 * 1000) // Shift forward 24 hours

    const message = new SiweMessage({
      domain: window.location.host,
      address,
      statement: SIWE_LOGIN_STATEMENT,
      uri: window.location.origin,
      version: '1',
      chainId,
      nonce: nonce,
      expirationTime: expires.toISOString(),
    })
    let signature
    try {
      signature = await signMessageAsync({
        message: message.prepareMessage(),
      })
    } catch (err: any) {
      setFetchStatus('error')
      if (err.name == 'UserRejectedRequestError') {
        console.log('User rejected signing')
        return
      }
      console.error('Signing error', err)
      return
    }

    // Verify signature
    const verifyRes = await fetch('/api/siwe-verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message, signature }),
    })
    if (!verifyRes.ok) {
      setFetchStatus('error')
      console.error('Fetch error', verifyRes)
      return
    }
    let rs = await verifyRes.json()
    if (!rs.ok) {
      setFetchStatus('error')
      console.error('Signature failed validation')
      return
    }

    // Verification succeeded.
    doUserCheck(dispatch) // Refresh which addresses are verified now
    if (typeof onSignedIn == 'function') onSignedIn()
    setFetchStatus('done')
  }

  // User clicked the log-out button
  const logOut: React.MouseEventHandler<any> = async function logOut(e) {
    e.preventDefault()
    setFetchStatus('pending')
    await fetch('/api/logout')

    if (dispatch) doUserCheck(dispatch) // Refresh which addresses are verified now
    if (typeof onSignedOut == 'function') onSignedOut()
    setFetchStatus('done')
  }

  if (status == 'error') {
    return (
      <section className="card-notice">
        <WarningIndicator message="Failed to authorize your user session" />
      </section>
    )
  }
  if (status == 'start' || status == 'pending') {
    return (
      <section className="card-notice">
        <LoadingIndicator message="Verifying user session..." />
      </section>
    )
  }

  if (verifiedAddresses.includes(address)) {
    return (
      <section className="card-notice">
        <p>
          You are verified as the owner of this wallet. You can view <Link href="/profile">your profile</Link> for more details. <button onClick={logOut}>Log Out</button>
        </p>
      </section>
    )
  }

  let loginStatusView: React.ReactNode
  if (fetchStatus == 'pending') {
    loginStatusView = <LoadingIndicator message="Signing in..." />
  } else {
    loginStatusView = <button onClick={signIn}>Sign In</button>
  }

  return (
    <section className="card-notice">
      <p>
        Your browser claims this is your address, but you have not verified that yet. Click the button below to sign a
        message with your Ethereum wallet to verify you&rsquo;re the owner (more details on the sign-in process{' '}
        <Link href="/docs/login">over here</Link>).
      </p>
      <div style={{ textAlign: 'center' }}>{loginStatusView}</div>
    </section>
  )
}

export default SignIn
