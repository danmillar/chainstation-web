import React, { useEffect, useState } from 'react'
import { API2_SERVER_ROOT, API_SERVER_ROOT, MOMENTS_ADDRESS, useFetchStatus } from 'lib/util'
import { multicall } from 'wagmi/actions'
import { OwnedMoonCat } from 'lib/types'
import { parseAbi } from 'viem'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import useTxStatus from 'lib/useTxStatus'

const ZWS = '\u200B'

const MOMENTS = {
  address: MOMENTS_ADDRESS as `0x${string}`,
  abi: parseAbi([
    'function listClaimableMoments(uint256 rescueOrder) external view returns (uint16[])',
    'function batchClaim(uint256[] momentIds, uint256[] rescueOrders) external',
  ]),
} as const

const MomentsToClaim = ({ addresses }: { addresses: `0x${string}`[] }) => {
  const [status, setStatus] = useFetchStatus()
  const { viewMessage, processTransaction } = useTxStatus()
  const [pendingClaims, setPendingClaims] = useState<Record<number, readonly number[]>>({})

  useEffect(() => {
    let ignore = false

    // Get a list of all MoonCats owned by this address, and see if they have any unclaimed Moments
    async function doWork() {
      setStatus('pending')
      // This implementation only queries MoonCats owned by the first verified address the user has
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${addresses[0]}`)
      if (!rs.ok) {
        console.error('Failed to fetch', rs)
        setStatus('error')
        return
      }

      const data = await rs.json()
      if (ignore) return
      const ownedMoonCats: OwnedMoonCat[] = data.ownedMoonCats
      // For each MoonCat owned, query to see which Moments are claimable by them
      const multicalls = ownedMoonCats.map(
        (mc: OwnedMoonCat) =>
          ({
            ...MOMENTS,
            functionName: 'listClaimableMoments',
            args: [BigInt(mc.rescueOrder)],
          } as const)
      )

      const claimable = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return
      let pendingClaims: { [rescueOrder: number]: readonly number[] } = {}
      for (let i = 0; i < ownedMoonCats.length; i++) {
        if (claimable[i].length > 0) {
          // This MoonCat has claimable Moments; add them to the output mapping
          pendingClaims[ownedMoonCats[i].rescueOrder] = claimable[i]
        }
      }

      setPendingClaims(pendingClaims)
      setStatus('done')
    }
    doWork()
    return () => {
      ignore = true
    }
  }, [addresses, setStatus])

  // Assemble a batch claim for all the pending claims found
  async function doClaims() {
    if (Object.keys(pendingClaims).length == 0) return
    let moonCats: bigint[] = []
    let moments: bigint[] = []
    for (let [rescueOrder, claimable] of Object.entries(pendingClaims)) {
      for (let momentId of claimable) {
        moonCats.push(BigInt(rescueOrder))
        moments.push(BigInt(momentId))
      }
    }
    let rs = await processTransaction({
      ...MOMENTS,
      functionName: 'batchClaim',
      args: [moments, moonCats],
    })
  }

  if (status == 'start' || status == 'pending') {
    return (
      <>
        <h2>MoonCat{ZWS}Moments</h2>
        <LoadingIndicator message="Enumerating Moments for you..." />
      </>
    )
  } else if (status == 'error') {
    return (
      <>
        <h2>MoonCat{ZWS}Moments</h2>
        <WarningIndicator message="Data fetch encountered an error" />
      </>
    )
  } else if (Object.keys(pendingClaims).length == 0) {
    // No owned MoonCats have any Moments to claim
    return null
  }

  return (
    <>
      <h2>MoonCat{ZWS}Moments</h2>
      <section className="card">
        <p>
          The following MoonCats you own have been part of specific Moments and so can claim the MoonCat{ZWS}Moment NFT
          for that event. Click the claim button to generate a transaction to claim all these Moments. Moment NFTs wil
          be delivered to the MoonCats themselves (will appear in their Purrse of owned NFTs).
        </p>
        <ul style={{ listStyleType: 'none', paddingLeft: 0, margin: '2rem 0' }}>
          {Object.entries(pendingClaims).map(([rescueOrder, claimable]) => (
            <li key={rescueOrder}>
              <picture>
                <img
                  src={`${API_SERVER_ROOT}/image/${rescueOrder}?scale=2&padding=3`}
                  alt={`MoonCat ${rescueOrder}`}
                  title={`MoonCat ${rescueOrder}`}
                  style={{ height: 40, verticalAlign: -12, paddingRight: 10 }}
                />
              </picture>
              <em>#{rescueOrder}</em>: {claimable.length == 1 ? 'Moment' : 'Moments'} {claimable.join(', ')}
            </li>
          ))}
        </ul>
        <p>
          <button onClick={doClaims}>Claim Moments</button>
        </p>
        {viewMessage}
      </section>
    </>
  )
}

export default MomentsToClaim
