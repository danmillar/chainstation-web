import React, { useContext } from 'react'
import { useRouter } from 'next/router'
import Icon from './Icon'
import { API_SERVER_ROOT, AppVisitorContext } from '../lib/util'
import { MoonCatData } from '../lib/types'
import Link from 'next/link'

interface ThumbProps {
  moonCat: MoonCatData
  className?: string
  thumbHandler?: (moonCat: MoonCatData) => React.ReactNode
  href?: string
  onClick?: (moonCat: MoonCatData, event: React.MouseEvent<HTMLElement, MouseEvent>) => any
}

const MoonCatThumb = ({ moonCat, className, thumbHandler, href, onClick }: ThumbProps) => {
  const router = useRouter()
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)

  let thumbStyle: React.CSSProperties = {}
  switch (viewPreference) {
    case 'accessorized':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5)`
      break
    case 'mooncat':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=)`
      break
    case 'face':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=&headOnly)`
      break
    case 'event':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/event-image/${moonCat.rescueOrder}?scale=3&padding=5)`
      break
  }

  let details: React.ReactNode
  if (thumbHandler) {
    details = thumbHandler(moonCat)
  } else {
    let name = null
    if (typeof moonCat.name != 'undefined') {
      if (moonCat.name === true || moonCat.name == '\ufffd') {
        name = (
          <p>
            <Icon name="question" />
          </p>
        )
      } else {
        name = <p>{moonCat.name.trim()}</p>
      }
    }

    details = (
      <>
        <p>#{moonCat.rescueOrder}</p>
        <p>
          <code>{moonCat.catId}</code>
        </p>
        {name}
      </>
    )
  }

  let classes = ['item-thumb']
  if (className) classes.push(className)

  if (href) {
    // Use a standard link
    return (
      <Link href={href}>
        <a className={classes.join(' ')}>
          <div className="thumb-img" style={thumbStyle} />
          {details}
        </a>
      </Link>
    )
  }

  if (typeof onClick != 'function') {
    // Use default link
    return (
      <Link href={`/mooncats/${moonCat.rescueOrder}`}>
        <a className={classes.join(' ')}>
          <div className="thumb-img" style={thumbStyle} />
          {details}
        </a>
      </Link>
    )
  }

  // Do a custom onClick function
  const style = {
    cursor: classes.includes('disabled') ? '' : 'pointer'
  }
  return (
    <div className={classes.join(' ')} style={style} onClick={(e) => onClick(moonCat, e)}>
      <div className="thumb-img" style={thumbStyle} />
      {details}
    </div>
  )
}
export default MoonCatThumb
