import { useEffect, useState } from 'react'
import { parseAbi } from 'viem'
import { multicall, readContract } from 'wagmi/actions'
import { ACCESSORIES_ADDRESS, switchToChain, useFetchStatus } from './util'

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as `0x${string}`,
  abi: parseAbi([
    'function totalAccessories() external view returns (uint256)',
    'function balanceOf(uint256 rescueOrder) external view returns (uint256)',
    'function ownedAccessoryByIndex(uint256 rescueOrder, uint256 ownedAccessoryIndex) external view returns ((uint232 accessoryId, uint8 paletteIndex, uint16 zIndex))',
    'function accessoryInfo(uint256 accessoryId) external view returns ((uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] positions, bool availableForPurchase, uint256 price))',
  ]),
} as const

export interface OwnedAccessory {
  accessoryId: bigint
  name: string
  paletteIndex: number
  availablePalettes: number
  isBackground: boolean
  zIndex: number
}

/**
 * Hook for fetching all owned Accessories for a specific MoonCat
 * Includes details about which ones are worn and palette options for each Accessory
 */
export default function useMoonCatAccessories(rescueOrder: number) {
  const [status, setStatus] = useFetchStatus()
  const [ownedAccessoryCount, setOwnedAccessoryCount] = useState<number>(0)
  const [ownedAccessories, setOwnedAccessories] = useState<OwnedAccessory[]>([])

  useEffect(() => {
    let ignore = false

    async function doWork() {
      setStatus('pending')
      if (!(await switchToChain(1))) {
        console.error('Failed to switch networks')
        setStatus('error')
        return
      }

      // Start with fetching how many Accessories this MoonCat owns
      const count = await readContract({
        ...ACCESSORIES,
        functionName: 'balanceOf',
        args: [BigInt(rescueOrder)],
      })
      if (ignore) return
      setOwnedAccessoryCount(Number(count))

      // Iterate up to that accessory count number to fetch which Accessory ID it is, and if it's currently worn by this MoonCat
      const multicalls = []
      for (let i = 0n; i < count; i++) {
        multicalls.push({
          ...ACCESSORIES,
          functionName: 'ownedAccessoryByIndex',
          args: [BigInt(rescueOrder), i],
        } as const)
      }
      let ownedAccessories = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      // Fetch additional detail about each Accessory that is owned
      const infoCalls = ownedAccessories.map(
        (ownedAcc) =>
          ({
            ...ACCESSORIES,
            functionName: 'accessoryInfo',
            args: [ownedAcc.accessoryId],
          } as const)
      )
      let accInfo = await multicall({ contracts: infoCalls, allowFailure: false })
      if (ignore) return

      let combinedDetails = ownedAccessories.map((ownedAcc, i) => {
        // Combined the owned-accessory info with the details about the accessory
        const info = accInfo[i]

        // Parse the metabyte info into a binary string
        const s = ('00000000' + info.metabyte.toString(2)).slice(-8)

        // Parse the Accessory Name into UTF8
        let nameBuffer = Buffer.from(info.name.substring(2), 'hex')
        let firstNull = nameBuffer.indexOf(0x00)
        if (firstNull >= 0) {
          nameBuffer = nameBuffer.subarray(0, firstNull)
        }

        return {
          ...ownedAcc,
          name: nameBuffer.toString('utf8'),
          availablePalettes: info.availablePalettes,
          isBackground: s[7] == '1',
        }
      })

      setOwnedAccessories(combinedDetails)
      setStatus('done')
    }
    doWork()

    return () => {
      ignore = true
    }
  }, [rescueOrder, setStatus])

  return {
    status,
    ownedAccessoryCount,
    ownedAccessories,
  }
}
