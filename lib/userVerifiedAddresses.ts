import { isSessionValid } from './firebase'
import { NextApiRequest } from 'next'

export default async function userVerifiedAddresses(req: NextApiRequest, chain: number) {
  const keyring = req.session.keyring

  if (!keyring || !Array.isArray(keyring) || keyring.length == 0) {
    // Keyring is missing/empty
    console.log('userVerifiedAddresses: User keyring is empty; nothing to validate')
    return []
  }

  const toCheck = keyring.filter((k) => k.siwe.chainId == chain)
  if (toCheck.length == 0) {
    // No Keys for this chain
    return []
  }

  let validKeys = []
  for (let k of toCheck) {
    const isValid = await isSessionValid(k.siwe, k.signature)
    if (isValid) {
      validKeys.push(k)
    } else {
      // No verified user session exists; user logged out and a stale session is trying to be used
      console.error('Stale session attempted for', k.siwe.address)
      console.error('forwarded-for', req.headers['x-forwarded-for'])
      console.error('remote address', req.socket.remoteAddress)
    }
  }
  return validKeys
}
