import { Moment, MoonCatData } from 'lib/types'
import { interleave } from './util'
import Link from 'next/link'
const moments: Moment[] = require('lib/moments_meta.json')

/**
 * Hue colors that are exactly in the middle of the range defined for that color name
 *
 * Each color range has two integers because hue values are truncated, so the degree
 * just below the exact value is also within one degree of that value.
 */
const trueHues: number[] = [
  359,
  0, // Red
  29,
  30, // Orange
  59,
  60, // Yellow
  89,
  90, // Chartreuse
  119,
  120, // Green
  149,
  150, // Teal
  179,
  180, // Cyan
  209,
  210, // SkyBlue
  239,
  240, // Blue
  269,
  270, // Purple
  299,
  300, // Magenta
  239,
  330, // Fuchsia
]

/**
 * Mapping for hue values that are on the edge of another color.
 *
 * The value of this mapping is the other color that it's close to.
 */
const edgeHues: { [key: number]: string } = {
  15: 'Orange', // is Red
  16: 'Red', // is Orange
  45: 'Yellow', // is Orange
  46: 'Orange', // is Yellow
  75: 'Chartreuse', // is Yellow
  76: 'Yellow', // is Chartreuse
  105: 'Green', // is Chartreuse
  106: 'Chartreuse', // is Green
  135: 'Teal', // is Green
  136: 'Green', // is Teal
  165: 'Cyan', // is Teal
  166: 'Teal', // is Cyan
  195: 'SkyBlue', // is Cyan
  196: 'Cyan', // is SkyBlue
  225: 'Blue', // is SkyBlue
  226: 'SkyBlue', // is Blue
  255: 'Purple', // is Blue
  256: 'Blue', // is Purple
  285: 'Magenta', // is Purple
  286: 'Purple', // is Magenta
  315: 'Fuchsia', // is Magenta
  316: 'Magenta', // is Fuchsia
  345: 'Red', // is Fuchsia
  346: 'Fuchsia', // is Red
}

const moonCatPopSpokesCats: { [rescueOrder: number]: { id: number; flavor: string } } = {
  0: { id: 0, flavor: 'Station Crasher' },
  2723: { id: 1, flavor: 'ZomBerry' },
  10484: { id: 2, flavor: 'Dapper' },
  3294: { id: 3, flavor: 'Pure 2017 Kitty Juice' },
  1756: {
    id: 4,
    flavor: 'Gm              Milk of Purring             Gn',
  },
  12693: { id: 5, flavor: '3 Star Garf' },
  1337: { id: 6, flavor: 'Moon-Mate' },
  2165: { id: 7, flavor: "Cosmonaut's Cold Ones" },
  1039: { id: 8, flavor: 'Garfield sips' },
  476: { id: 9, flavor: "Day1 Alien's Favorite" },
  217: { id: 10, flavor: 'Black as Midnight on a Moonless Night' },
  1117: { id: 11, flavor: 'White Genesis Pop' },
  15401: { id: 12, flavor: 'Kuipurr Belt Kool' },
  5766: { id: 13, flavor: 'Glamour Puss' },
  20451: { id: 14, flavor: 'Parallax Alien' },
  24673: { id: 15, flavor: 'Six-Nipped Golden Kitten Attack Ale  ' },
  2726: { id: 16, flavor: 'Stellar Starberry' },
  24704: { id: 17, flavor: 'Milky Whey' },
  527: { id: 18, flavor: 'The Invisible Drink' },
  39: { id: 19, flavor: "Whiskers' Astro Ginger Magic Ice" },
  18074: { id: 20, flavor: 'Funky Fruit Fizz' },
  1289: { id: 21, flavor: 'Midnight Blue Raspberry Stardust' },
  1069: { id: 22, flavor: 'Meowtain Purrfect Pawnch' },
  6939: { id: 23, flavor: 'Catty Refresher' },
  14949: { id: 24, flavor: "Alien's Tripel Speciale" },
  6: { id: 25, flavor: "mister moo's vegas vacation" },
  9: {
    id: 26,
    flavor: '.                  #9 Dream                   Gn',
  },
  19507: { id: 27, flavor: "Mather's Sour Pucker" },
  41: { id: 28, flavor: 'Coke Royal Mint Nr.41 by CRL' },
  1514: { id: 29, flavor: 'Hydra-tion Station' },
  1218: { id: 30, flavor: 'Tropical Titan + Saturn Splash' },
  13358: { id: 31, flavor: "80's Elixir" },
  23677: { id: 32, flavor: 'Catsymalist Fizzy Sparkle' },
  2665: { id: 33, flavor: 'WAGMI' },
  75: { id: 34, flavor: "Blueberry's Royal Elixir" },
  3138: { id: 35, flavor: 'Midnight Meowjito' },
  24234: { id: 36, flavor: 'TokenAde' },
  1070: { id: 37, flavor: 'Ammonia Blast' },
  10: { id: 38, flavor: "Collector's Edition" },
  18189: { id: 39, flavor: 'Pixel Star' },
  13349: { id: 40, flavor: 'Panther Milk' },
  1264: { id: 41, flavor: 'buff cat' },
  302: { id: 42, flavor: 'Day 1' },
  19604: { id: 43, flavor: 'Triple Pixel Punch' },
  17970: { id: 44, flavor: "Paws' Bubbly Refresher" },
  453: { id: 45, flavor: 'Blueberry Bantha Milk' },
  3632: { id: 46, flavor: 'Paw-Brewed Pixel' },
  5670: { id: 47, flavor: 'StellaCat Soda' },
  9665: { id: 48, flavor: "Garfield's Lasagna - Nutrient Shake" },
  283: { id: 49, flavor: 'Lion Malt' },
  1382: { id: 50, flavor: "Frida's Feline Fresca" },
  6755: { id: 51, flavor: 'Who Loves Orange Soda?!' },
  13872: { id: 52, flavor: 'CopyCat Cola' },
  19373: { id: 53, flavor: 'Gutter Juice Cola' },
  20800: { id: 54, flavor: 'Tuna Moon Juice' },
  403: { id: 55, flavor: 'Day One Daiquiri' },
  3204: { id: 56, flavor: '2017 Fizzy Bubbly' },
  86: { id: 57, flavor: 'GENESIS KAIJU PURE BLACK' },
  1684: { id: 58, flavor: 'Void Slurp' },
  12: { id: 59, flavor: 'Thirst Drink Panther' },
  22313: { id: 60, flavor: 'Cherry Mecha' },
  22: { id: 61, flavor: "Doc Wander's Patent Ponderade" },
  3137: { id: 62, flavor: 'Meowberry Purrfection' },
  8125: { id: 63, flavor: 'probably nothing serum' },
  899: { id: 64, flavor: 'formula 899' },
  7500: { id: 65, flavor: 'gm' },
  13675: { id: 66, flavor: 'Milky Way Juice' },
  14737: { id: 67, flavor: 'WizardX style' },
  419: { id: 68, flavor: 'MeowTwo #419' },
  24335: { id: 69, flavor: 'Diet Scratching Post Punch' },
  1829: { id: 70, flavor: 'Magic Bus Brew' },
  13097: { id: 71, flavor: 'Sedating Refresher' },
  2710: { id: 72, flavor: 'Red Moon Dew' },
  7265: { id: 73, flavor: 'Popsi Purrfect' },
  526: { id: 74, flavor: 'Pure Genesis MoonCat #526' },
  19533: { id: 75, flavor: 'Machine Cat' },
  635: { id: 76, flavor: 'inversebrah' },
  6612: { id: 77, flavor: 'Moon Rum' },
  3028: { id: 78, flavor: 'Fuchsia Meow-garita' },
  2807: { id: 79, flavor: "Orangebeard's Delight" },
  4723: { id: 80, flavor: '- Drink Me -' },
  15508: { id: 81, flavor: "Salem's Soiree Seltzer" },
  11120: { id: 82, flavor: "Kepler's Supurrnova" },
  11676: { id: 83, flavor: 'Paw-Brewed Triple-Nipped Cougar Pop' },
}

function earlyRescue(mc: MoonCatData) {
  if (mc.rescueOrder <= 491) {
    return 'First Day'
  } else if (mc.rescueOrder <= 1568) {
    return 'First Week'
  }
  return false
}

function yearStart(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 0:
      return '2017'
    case 3365:
      return '2018'
    case 5684:
      return '2019'
    case 5755:
      return '2020'
    case 5758:
      return '2021'
  }
  return false
}

function yearEnd(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 3364:
      return '2017'
    case 5683:
      return '2018'
    case 5754:
      return '2019'
    case 5757:
      return '2020'
    case 25439:
      return '2021'
  }
  return false
}

export default function getMoonCatTidbits(moonCat: MoonCatData): { [key: string]: boolean | React.ReactNode } {
  let bytes = []
  for (let i = 2; i < moonCat.catId.length; i += 2) {
    bytes.push(parseInt(moonCat.catId.slice(i, i + 2), 16))
  }

  const hexRepeats = moonCat.catId.slice(2).match(/([a-f0-9])\1{2,}/gi)
  const glowSum = bytes[2] + bytes[3] + bytes[4]

  let inMoments = []
  for (let moment of moments) {
    if (moment.moonCats.includes(moonCat.rescueOrder)) {
      inMoments.push(moment.momentId)
    }
  }
  let momentDetail: React.ReactNode = false
  if (inMoments.length == 1) {
    momentDetail = <Link href={'/moments/' + inMoments[0]}>{'Moment ' + inMoments[0]}</Link>
  } else if (inMoments.length > 1) {
    momentDetail = (
      <>
        Moments{' '}
        {interleave(
          inMoments.map((id) => (
            <Link key={id} href={'/moments/' + id}>
              {id}
            </Link>
          )),
          ', '
        )}
      </>
    )
  }

  const pop = moonCatPopSpokesCats[moonCat.rescueOrder]
  const spokesCat =
    typeof pop != 'undefined' ? (
      <a href={'https://pop.mooncat.community/vm/' + pop.id} rel="noreferrer" target="_blank">
        {pop.flavor}
      </a>
    ) : (
      false
    )

  return {
    numericId: /^[0-9]+$/.test(moonCat.catId.slice(2)),
    alphaId: /^[a-f]+$/i.test(moonCat.catId.slice(4)),
    repeatId: hexRepeats == null ? false : hexRepeats.join(', '),
    earlyRescue: earlyRescue(moonCat),
    yearStart: yearStart(moonCat),
    yearEnd: yearEnd(moonCat),
    earlyNaming: typeof moonCat.namedOrder != 'undefined' && moonCat.namedOrder <= 368,
    trueColor: trueHues.includes(moonCat.hueInt),
    edgeColor: typeof edgeHues[moonCat.hueInt] == 'undefined' ? false : edgeHues[moonCat.hueInt],
    darkGlow: glowSum < 180 ? `${glowSum}/765 brightness` : false,
    brightGlow: glowSum > 585 ? `${glowSum}/765 brightness` : false,
    inMoments: momentDetail,
    spokesCat: spokesCat,
  }
}
