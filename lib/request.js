const https = require('https')
const http = require('http')

/**
 * Promise-ified version of https.request
 */
module.exports = function request(url, options = {}, postData) {
  if (typeof url == 'string') {
    url = new URL(url)
  }
  return new Promise((resolve, reject) => {
    let req
    let resultHandler = (res) => {
      let result = {
        httpVersion: res.httpVersion,
        httpStatusCode: res.statusCode,
        headers: res.headers,
        body: [],
      }

      res.on('data', (chunk) => {
        //console.log(`Got data... ${chunk.length}`);
        result.body.push(chunk)
      })
      res.on('end', () => {
        result.body = Buffer.concat(result.body)
        resolve(result)
      })
      res.on('abort', () => {
        resolve(result)
      })
      res.on('timeout', () => {
        req.abort()
        reject('timeout')
      })
    }

    if (url.protocol == 'https:') {
      req = https.request(url, options, resultHandler)
    } else {
      req = http.request(url, options, resultHandler)
    }

    req.on('error', (e) => {
      reject(e)
    })
    if (typeof postData !== 'undefined') {
      req.write(postData)
    }
    req.end()
  })
}
