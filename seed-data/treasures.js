require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const collection = db.collection('treasures')

  // Insert some sample Treasures into the 'treasures' collection
  await collection.doc().set({
    type: 'search',
    label: 'Stuff We All Get',
    icon: null,
    details: 'A sample gift that all MoonCats are eligible for.',
    ipfs: 'bafybeig2ea5p2xw2d5c552x3ocxj2xavsb6nglwczv2hyza7yhu2wvny5a',
    criteria: {
      moonCat: [],
    },
  })

  await collection.doc().set({
    type: 'search',
    label: 'Warm Wonders',
    icon: null,
    details: 'A fun extra that only warm-colored MoonCats are eligible for.',
    ipfs: 'bafybeig2ea5p2xw2d5c552x3ocxj2xavsb6nglwczv2hyza7yhu2wvny5a',
    criteria: {
      moonCat: [{ hue: 'red' }, { hue: 'orange' }, { hue: 'yellow' }],
    },
  })

  await collection.doc().set({
    type: 'mapped',
    label: 'Pallindrome Pals',
    icon: null,
    details: 'If a MoonCat has a specific pallindrome for a rescue order, they get something special.',
    mapping: {
      moonCat: {
        1001: 'bafybeig2ea5p2xw2d5c552x3ocxj2xavsb6nglwczv2hyza7yhu2wvny5a',
        2002: 'bafybeifnwnnyi4ddntzmlo3fn3a5n2hedu5ulysl2xfmac4x5soreeaxdu',
        3003: 'bafybeidexf6rw7s2dvrddqkwtzjqs5esuoope7iqvyxdcsll7qswnzqfua',
        4004: 'bafybeiawcrs4cnwj4wglynurogop34qan7o7cqq7odzykioys45k5fyosi',
        5005: 'bafybeiclp4inwmfeyyqc5sleljnb4xbbl7u4nbcg3dqudrklgbi7u6if6y',
        6006: 'bafybeica2y3qpnctrzm6jpljfmm7a5knzcla4knyi3nblveypc3shavvem',
        7007: 'bafybeig2ea5p2xw2d5c552x3ocxj2xavsb6nglwczv2hyza7yhu2wvny5a',
        8008: 'bafybeifnwnnyi4ddntzmlo3fn3a5n2hedu5ulysl2xfmac4x5soreeaxdu',
        9009: 'bafybeidexf6rw7s2dvrddqkwtzjqs5esuoope7iqvyxdcsll7qswnzqfua',
      },
    },
  })

  console.log('Done!')
})()
