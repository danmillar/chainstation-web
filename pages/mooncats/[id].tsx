import React, { CSSProperties } from 'react'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { MoonCatData, MoonCatDetails } from 'lib/types'
import { API_SERVER_ROOT } from 'lib/util'
import useTabHandler from 'lib/useTabHandler'
import MoonCatTabInfo from 'components/MoonCatTabInfo'
import MoonCatTabAccessories from 'components/MoonCatTabAccessories'
import MoonCatTabNfts from 'components/MoonCatTabNfts'
import MoonCatTabPhotobooth from 'components/MoonCatTabPhotobooth'
import MoonCatTabTimeline from 'components/MoonCatTabTimeline'
import Icon from 'components/Icon'
import { getAddress } from 'viem'

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export interface TabProps {
  moonCat: MoonCatData
  details: MoonCatDetails
}

interface Tab {
  label: string
  content: (props: TabProps) => JSX.Element
}
const tabs = {
  'info': {
    label: 'Info',
    content: MoonCatTabInfo,
  } as Tab,
  'accessories': {
    label: 'Accessories',
    content: MoonCatTabAccessories,
  } as Tab,
  'nfts': {
    label: 'NFTs',
    content: MoonCatTabNfts,
  } as Tab,
  'photobooth': {
    label: 'Photobooth',
    content: MoonCatTabPhotobooth,
  },
  'timeline': {
    label: 'Timeline',
    content: MoonCatTabTimeline,
  } as Tab,
}
type TabName = keyof typeof tabs
const defaultTab = 'info'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

interface Props {
  moonCat: MoonCatData
  details: MoonCatDetails | null
}

/**
 * Show detail page for a specific MoonCat
 * Static metadata for the MoonCat is passed in as a property.
 */
const MoonCatDetail: NextPage<Props> = ({ moonCat, details }) => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)

  const pageTitle =
    moonCat.name && moonCat.name !== '\ufffd'
      ? `MoonCat #${moonCat.rescueOrder}: ${moonCat.name}`
      : `MoonCat #${moonCat.rescueOrder} (${moonCat.catId})`

  let description
  let type = moonCat.genesis ? 'Genesis MoonCat' : 'MoonCat'
  if (details == null) {
    // Loading details failed; give summary data
    description = `An adorable ${ucFirst(moonCat.pattern)} ${type}, rescued in ${moonCat.rescueYear}`
  } else {
    let coatDetail = `${ucFirst(details.hue)} ${ucFirst(moonCat.pattern)}`
    if (!details.genesis && details.isPale) coatDetail = 'Pale ' + coatDetail
    description = `An adorable ${coatDetail} ${type}, rescued in ${moonCat.rescueYear}`
  }

  let heroImgStyle: CSSProperties = { maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }
  if (details && details.glow.reduce((sum, n) => sum + n, 0) < 180) {
    // If the MoonCat has a very dark glow, lighten the background behind them just a bit, to make it more clear
    heroImgStyle.filter = 'drop-shadow(0px 0px 120px rgba(255,255,255, 0.35))'
  }

  let bodyContent: React.ReactNode
  if (details == null) {
    bodyContent = (
      <p className="message-error">
        <Icon name="warning" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
        Failed to fetch MoonCat info
      </p>
    )
  } else {
    const TabContent = isTabName(activeTab) ? tabs[activeTab].content : tabs[defaultTab].content
    bodyContent = <TabContent moonCat={moonCat} details={details} />
  }

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <link rel="canonical" href={`https://chainstation.mooncatrescue.com/mooncats/${moonCat.rescueOrder}`} />
        <meta property="og:title" content={pageTitle} />
        <meta name="description" property="og:description" content={description} />
        <meta property="og:image" content={`${API_SERVER_ROOT}/image/${moonCat.catId}`} />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image" content={`${API_SERVER_ROOT}/cat-image/${moonCat.catId}`} />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image" content={`${API_SERVER_ROOT}/face-image/${moonCat.catId}`} />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <nav className="breadcrumb">
        <Link href="/mooncats">
          <a>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all MoonCats
          </a>
        </Link>
      </nav>
      <h1 className="hero">MoonCat #{moonCat.rescueOrder}</h1>
      <div style={{ margin: '2rem 0' }}>
        <picture>
          <img
            src={`${API_SERVER_ROOT}/image/${moonCat.catId}`}
            alt={`MoonCat #${moonCat.rescueOrder} (${moonCat.catId})`}
            style={heroImgStyle}
          />
        </picture>
      </div>
      <nav className="tabs">
        {Object.entries(tabs).map((tab) => (
          <div key={tab[0]} className={tab[0] == activeTab ? 'active' : ''} onClick={() => toggleTab(tab[0])}>
            {tab[1].label}
          </div>
        ))}
      </nav>
      <section className="tab-contents">{bodyContent}</section>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')
  const moonCatTraits: Array<MoonCatData> = require('lib/mooncat_traits.json')

  if (typeof ctx.params == 'undefined') {
    console.log('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  function getById(catId: string) {
    let targetId = catId.toLowerCase()
    for (let i = 0; i < moonCatTraits.length; i++) {
      if (moonCatTraits[i].catId.toLowerCase() == targetId) return moonCatTraits[i]
    }
    return null
  }

  let moonCat = null
  if (id.substring(0, 2) == '0x') {
    // ID is a hex ID
    moonCat = getById(id)
  } else if (!isNaN(Number(id))) {
    // ID is the rescue order
    moonCat = moonCatTraits[Number(id)]
  }
  if (!moonCat) {
    // No MoonCat with that identifier; 404
    return { notFound: true }
  }

  // Fetch additional details from the API server
  let details = null
  try {
    let rs = await request(`${API_SERVER_ROOT}/traits/${moonCat.rescueOrder}`)
    let jsonData = JSON.parse(rs.body.toString('utf8'))
    details = jsonData.details
    if (details.owner) {
      // Old-wrapped MoonCats are not enumerated
      details.owner = getAddress(details.owner) // Ensure address is in checksummed format
    }
    if (details.rescuedBy) {
      details.rescuedBy = getAddress(details.rescuedBy) // Ensure address is in checksummed format
    }
  } catch (err) {
    console.error('Failed to fetch details', err)
  }
  return { props: { moonCat, details } }
}

export default MoonCatDetail
