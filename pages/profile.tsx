import React, { useContext } from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import { AppVisitorContext, interleave, useIsMounted } from 'lib/util'
import { useAccount } from 'wagmi'
import SignIn from 'components/SignIn'
import EthereumAddress from 'components/EthereumAddress'
import MomentsToClaim from 'components/MomentsToClaim'
import TreasuresList from 'components/TreasuresList'

const ZWS = '\u200B'
const pageTitle = 'Profile'
const Profile: NextPage = () => {
  const isMounted = useIsMounted()
  const { address, isConnected } = useAccount()
  const {
    state: {
      verifiedAddresses: { value: verifiedAddresses },
    },
  } = useContext(AppVisitorContext)

  if (verifiedAddresses.length == 0) {
    let bodyView
    if (isMounted && isConnected && typeof address != 'undefined') {
      bodyView = <SignIn address={address} />
    } else if (isMounted && !isConnected) {
      bodyView = (
        <section className="card-help">
          <p>
            You are not currently connected; click the &ldquo;Connect&rdquo; button in the bottom-left to connect your
            Ethereum wallet to this site.
          </p>
        </section>
      )
    }
    return (
      <div id="content-container">
        <Head>
          <title>{pageTitle}</title>
          <meta property="og:title" content={pageTitle} />
          <meta
            name="description"
            property="og:description"
            content="Your own user profile as a user and owner of MoonCatRescue ecosystem assets"
          />
        </Head>
        <div className="text-container">
          <h1 className="hero">My Profile</h1>
          {bodyView}
        </div>
      </div>
    )
  }

  const addressDisplay = interleave(
    verifiedAddresses.map((a) => <EthereumAddress key={a} address={a} />),
    ', '
  )
  const addressPlural = verifiedAddresses.length > 1 ? 'those addresses' : 'that address'

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Your own user profile as a user and owner of MoonCatRescue ecosystem assets"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">My Profile</h1>
        <section className="card">
          <p>
            You are known and verified on this site as {addressDisplay}. Below are different tools and information that
            are only available to you as a verified owner of {addressPlural}.
          </p>
        </section>
        <MomentsToClaim addresses={verifiedAddresses} />
        <h2>Treasures</h2>
        <section className="card">
          <p>
            There are many projects and products that find a home in ChainStation Alpha. And it being an open and
            collaborative space, many merchants, traders, and other creators like giving gifts to other denizens of the
            space. Here&rsquo;s the treasures you as an Etherean have access to:
          </p>
        </section>
        <TreasuresList />
      </div>
    </div>
  )
}
export default Profile
