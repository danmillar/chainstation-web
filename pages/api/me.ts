import { withIronSessionApiRoute } from 'iron-session/next'
import { NextApiRequest, NextApiResponse } from 'next'
import ironOptions from 'lib/ironOptions'
import { SiweMessage } from 'siwe'
import userVerifiedAddresses from 'lib/userVerifiedAddresses'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method } = req
  const finalize = (rs: SiweMessage['address'][]) => {
    res.send({ addresses: rs })
  }
  switch (method) {
    case 'GET':
      const targetChain = 1 // Currently all activity happens on the main chain
      const validKeys = await userVerifiedAddresses(req, targetChain)

      // Replace user's local state with only valid keys
      if (validKeys.length == 0) {
        req.session.destroy()
      } else {
        req.session.keyring = validKeys
        await req.session.save()
      }
      return finalize(validKeys.map((k) => k.siwe.address!))
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
      return
  }
}

export default withIronSessionApiRoute(handler, ironOptions)
