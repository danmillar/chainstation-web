import type { NextApiRequest, NextApiResponse } from 'next'
const request = require('lib/request')

type TokenName = 'mooncat' | 'mcat17'
type AdoptType = 'random' | 'specific'
type CacheKey = `${TokenName}-${AdoptType}`

let cachedValues: { [key in CacheKey as string]: any } = {}
let cachedDates: { [key in CacheKey as string]: Date } = {}
const CACHE_LIFETIME = 30

const isTokenName = (t: string): t is TokenName => {
  switch (t) {
    case 'mooncat':
    case 'mcat17':
      return true
    default:
      return false
  }
}
const isAdoptType = (t: string): t is AdoptType => {
  switch (t) {
    case 'random':
    case 'specific':
      return true
    default:
      return false
  }
}

function finalize(token: CacheKey, res: NextApiResponse) {
  if (typeof cachedDates[token] === 'undefined') {
    // Need to return, but don't have a cached value
    res.status(502).end()
    return
  }
  const cachedDate = cachedDates[token]
  const cachedValue = cachedValues[token]

  res.setHeader('Last-Modified', cachedDate.toUTCString())
  res.setHeader('Cache-Control', 'public, s-maxage=30, max-age=120, stale-while-revalidate=120, stale-if-error=3600')
  res.status(200).json({
    'lastModified': Math.floor(cachedDate.getTime() / 1000),
    'WETH': {
      calldata: cachedValue.data,
      sellAmount: cachedValue.sellAmount,
    },
  })
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method, query } = req
  switch (method) {
    case 'GET':
      const tokenId = query.id
      if (typeof tokenId == 'undefined' || Array.isArray(tokenId)) {
        res.status(400).end()
        return
      }
      const [tokenName, adoptType] =
        tokenId.indexOf('-') > 0 ? tokenId.toLowerCase().split('-') : [tokenId.toLowerCase(), 'specific']
      if (!isTokenName(tokenName) || !isAdoptType(adoptType)) {
        res.status(400).end()
        return
      }
      const token: CacheKey = (tokenName + '-' + adoptType) as CacheKey
      const cachedDate = cachedDates[token]

      const now = new Date()
      now.setMilliseconds(0)
      if (typeof cachedDate !== 'undefined' && now.getTime() - cachedDate.getTime() < CACHE_LIFETIME * 1000) {
        // Cache exists and is fresh; use the cached value for response
        const ifModified = req.headers['if-modified-since'] ? new Date(req.headers['if-modified-since']) : false
        if (ifModified && cachedDate.getTime() <= ifModified.getTime()) {
          // Request has supplied an If-Modified-Since header, and the cache is older than that, so no response needed
          res.status(304).end()
          return
        }
        finalize(token, res)
        return
      }

      // Cache doesn't exist or isn't fresh; try to fetch new
      let jsonData = null
      try {
        let tokenAddress: `0x${string}` | false = false
        let vaultId: number | false = false
        let tokenAmount: string | false = false
        if (tokenName == 'mooncat') {
          tokenAddress = '0x98968f0747E0A261532cAcC0BE296375F5c08398'
          vaultId = 25
        } else if (tokenName == 'mcat17') {
          tokenAddress = '0xa8b42c82a628dc43c2c2285205313e5106ea2853'
          vaultId = 451
        }
        if (adoptType == 'specific') {
          tokenAmount = '1030000000000000000'
        } else if (adoptType == 'random') {
          tokenAmount = '1020000000000000000'
        }
        if (tokenAddress === false || tokenAmount === false) {
          // Invalid token request
          res.status(400).end()
          return
        }
        let rs = await request(
          `https://api.0x.org/swap/v1/quote?buyToken=${tokenAddress}&sellToken=WETH&buyAmount=${tokenAmount}&slippagePercentage=0.01`,
          {
            headers: { '0x-api-key': process.env['ZEROX_API_KEY'] },
          }
        )
        const body = rs.body.toString('utf8')
        jsonData = JSON.parse(body)
        if (!jsonData) {
          console.error('Failed to parse 0x return value:', body)
          finalize(token, res)
          return
        }
      } catch (err) {
        console.error('Failed to connect to 0x', err)
        finalize(token, res)
        return
      }

      // Successfully got new data; cache it
      cachedValues[token] = jsonData
      cachedDates[token] = now
      finalize(token, res)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
      return
  }
}
