import { withIronSessionApiRoute } from 'iron-session/next'
import { NextApiRequest, NextApiResponse } from 'next'
import ironOptions from 'lib/ironOptions'
import { getAppFirestore, isSessionValid } from 'lib/firebase'
import { USER_SESSION_COLLECTION } from 'lib/util'

const db = getAppFirestore()

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method } = req
  const finalize = () => {
    res.send({ ok: true })
  }
  switch (method) {
    case 'GET':
      // Currently all activity happens on the main chain
      const targetChain = 1

      const keyring = req.session.keyring
      if (!keyring || !Array.isArray(keyring) || keyring.length == 0) {
        // Keyring is missing/empty; nothing to do
        return finalize()
      }

      // A GET request removes ALL addresses on that chain
      let newKeyring = []
      for (let k of keyring) {
        // Keep keys for other chains
        if (k.siwe.chainId != targetChain) {
          newKeyring.push(k)
          continue
        }

        // This key is for the target chain; verify it
        if (!k.signature || !k.siwe) {
          // Incomplete session data (older version of app?)
          continue
        }

        const isValid = await isSessionValid(k.siwe, k.signature)
        if (!isValid) continue

        // User is currently holding a valid session for a specific address
        // Therefore they are authorized to log this address out on the database-side,
        // which will log out all other sessions
        const sessionRef = db.collection(USER_SESSION_COLLECTION).doc(`${k.siwe.address}-${k.siwe.chainId}`)
        await sessionRef.delete()

        // This session was valid, but is now logged-out; don't add it to the keyring to keep
      }

      if (newKeyring.length == 0) {
        req.session.destroy()
      } else {
        req.session.keyring = newKeyring
        await req.session.save()
      }
      return finalize()
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}

export default withIronSessionApiRoute(handler, ironOptions)
