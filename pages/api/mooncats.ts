import { MoonCatData } from 'lib/types'
import type { NextApiRequest, NextApiResponse } from 'next'
import { queryToFilterSettings, filterMoonCatList } from 'lib/util'
import _rawTraits from 'lib/mooncat_traits.json'

export type ResponseData = {
  length: number
  totalLength: number
  moonCats: MoonCatData[]
}

export default function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  // Build MoonCatFilters object from query parameters
  let inputDataObject = req.method == 'GET' ? req.query : req.body
  const filters = queryToFilterSettings(inputDataObject)
  if (filters == null) return res.status(400).end()

  let totalList: MoonCatData[] = _rawTraits as MoonCatData[]
  if (typeof inputDataObject.mooncats !== 'undefined' && inputDataObject.mooncats !== 'all') {
    let moonCatArray = Array.isArray(inputDataObject.mooncats)
      ? inputDataObject.mooncats
      : inputDataObject.mooncats.split(',')
    let requestedMoonCats = moonCatArray.map((str: string) => parseInt(str))
    totalList = totalList.filter((moonCat) => {
      return requestedMoonCats.includes(moonCat.rescueOrder)
    })
  }

  // Filter the list of MoonCats as the request indicates
  let filteredList = filterMoonCatList(totalList, filters)

  // Slice to the desired offset and limit
  const limit = !inputDataObject.limit ? 50 : parseInt(String(inputDataObject.limit))
  if (limit > 200) return res.status(400).end()
  const offset = !inputDataObject.offset ? 0 : parseInt(String(inputDataObject.offset))

  res.status(200).json({
    length: filteredList.length,
    totalLength: totalList.length,
    moonCats: filteredList.slice(offset, offset + limit),
  })
}
