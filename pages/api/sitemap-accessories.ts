import type { NextApiRequest, NextApiResponse } from 'next'
const { SitemapStream } = require('sitemap')

/**
 * Generate an XML sitemap listing out all Accessories by ID
 */
export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method } = req
  switch (method) {
    case 'GET':
      res.setHeader('Content-Type', 'application/xml')
      try {
        const smStream = new SitemapStream({ hostname: 'https://chainstation.mooncatrescue.com/' })

        for (let accessoryId = 0; accessoryId <= 1000; accessoryId++) {
          smStream.write({ url: `/accessories/${accessoryId}` })
        }
        smStream.pipe(res).on('error', (e: any) => {
          throw e
        })
        smStream.end()
      } catch (e) {
        console.error(e)
        res.status(500).end()
      }
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
      return
  }
}
