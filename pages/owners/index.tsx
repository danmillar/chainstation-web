import React from 'react'
import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import { ADDRESS_DETAILS, API2_SERVER_ROOT } from 'lib/util'
import type { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { Address } from 'wagmi'
import EthereumAddress from 'components/EthereumAddress'

const ZWS = '\u200B'

interface OwnerSummary {
  address: `0x${string}`
  ownedMoonCats: number
}
interface Props {
  moonCatOwners: OwnerSummary[]
}

const Owners: NextPage<Props> = ({ moonCatOwners }) => {
  const router = useRouter()

  async function doLookup(address: Address) {
    router.push(`/owners/${address}`)
  }

  let largeHolders = moonCatOwners.filter((o) => typeof ADDRESS_DETAILS[o.address] == 'undefined')
  let targetNumber = largeHolders[20].ownedMoonCats
  largeHolders = largeHolders.filter((o) => o.ownedMoonCats >= targetNumber)

  const pageTitle = 'Owner Profiles'

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta name="description" property="og:title" content="Gallery view of individual Ethereum address holdings" />
      </Head>
      <div className="text-container">
        <h1 className="hero">Owner Profiles</h1>
        <RandomMoonCatRow />
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} onValidate={doLookup} />
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
        <section className="card">
          <h2>NFT Pools</h2>
          <div style={{ display: 'flex', flexWrap: 'wrap', gap: '1em' }}>
            <ul style={{ flex: '0 1 30%', boxSizing: 'border-box', margin: 0, minWidth: '15em' }}>
              {Object.entries(ADDRESS_DETAILS)
                .filter(([k, v]) => {
                  return v.type == 'pool'
                })
                .map(([k, v]) => {
                  return (
                    <li key={k}>
                      <Link href={`/owners/${k}`}>{v.label}</Link>
                    </li>
                  )
                })}
            </ul>
            <p style={{ flex: '1 1', minWidth: '20em' }}>
              Each of these addresses are NFT pools, so all the MoonCats in them are up for adoption <em>right now</em>.
            </p>
          </div>
          <h2>Lots of Friends!</h2>
          <p>Want to see a bunch of MoonCats all in one place? Check out the number of adoptees these owners have:</p>
          <div style={{ columnWidth: '20em', marginBottom: '1.5em' }}>
            <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto' }}>
              <tbody>
                {largeHolders.map((o) => {
                  return (
                    <tr key={o.address}>
                      <td style={{ fontSize: '0.8rem' }}>
                        <EthereumAddress address={o.address} />
                      </td>
                      <td style={{ padding: '0.2rem 1rem', textAlign: 'right' }}>{o.ownedMoonCats}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
          <p>
            Total MoonCats owned includes original and Acclimated MoonCats. Owners of MoonCats in other wrappers can{' '}
            <a href="https://mooncat.community/acclimator">visit the Acclimator</a> to get them included in these
            counts.
          </p>
        </section>
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')
  let data = await request(`${API2_SERVER_ROOT}/owner-profile?sort=mooncats&limit=30`)
  let owners: OwnerSummary[] = JSON.parse(data.body.toString('utf8'))

  return {
    props: {
      moonCatOwners: owners,
    },
  }
}

export default Owners
