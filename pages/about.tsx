import RandomMoonCatRow from 'components/RandomMoonCatRow'
import NFTXAdopt from 'components/NFTXAdopt'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'

const ZWS = '\u200B'

interface Props {}

const Home: NextPage<Props> = ({}) => {
  const pageTitle = `MoonCat${ZWS}Rescue`
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Playground of the colorful felines rescued from the moon"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">{pageTitle}</h1>
        <div style={{ textAlign: 'center' }}>
          <iframe
            src="https://www.youtube.com/embed/Q7EB-diG_9w"
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowFullScreen
            style={{ width: 800, height: 450, maxWidth: '95%', border: '0' }}
          ></iframe>
        </div>

        <RandomMoonCatRow />
        <section className="card-help">
          <p>
            The MoonCat{ZWS}Rescue project brought a collection of colorful, space-dwelling felines to the blockchain to
            be <em>cute</em>, <em>playful companions</em>, and <strong>push the boundaries</strong> of what the Ethereum
            ecosystem and blockchain technologies in general could do.
          </p>
          <p>
            The project launched in 2017 (before even the term &ldquo;NFT&rdquo; was well-defined), and in the
            intervening years has grown to bring these virtual pets to many parts of the modern Ethereum space (a.k.a.
            &ldquo;ChainStation Alpha&rdquo;)
          </p>
        </section>
      </div>
      <section
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'space-evenly',
          gap: '2rem',
          maxWidth: '90em',
          margin: '0 auto',
        }}
      >
        <section className="card" style={{ flex: '1 1 22em' }}>
          <h2>Adopt a MoonCat</h2>
          <p>
            Want a MoonCat <em>right now</em>? Click the button below to adopt a MoonCat right away!
          </p>
          <NFTXAdopt quoteType="mooncat-random" style={{ marginBottom: '1.5em' }} />
          <p>
            This will grab you a random MoonCat from the{' '}
            <Link href="/owners/0x98968f0747e0a261532cacc0be296375f5c08398">NFTX pool</Link>. If you prefer to pick a
            specific one yourself, visit that pool&rsquo;s profile page, or{' '}
            <a href="https://app.uniswap.org/nfts/collection/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69">
              browse across other NFT marketplaces
            </a>
            .
          </p>
        </section>
        <section className="card" style={{ flex: '1 1 22em' }}>
          <h2>MoonCat Plushie</h2>
          <p>
            Want to give a MoonCat a real-world hug? Now you can! The first run of MoonCat plushies is going on now.
          </p>
          <section className="sub-card" style={{ marginBottom: '1.5em' }}>
            <p style={{ marginBottom: '1rem' }}>
              <picture>
                <img
                  src="/img/plush1.webp"
                  style={{ maxHeight: 200, maxWidth: '40%', paddingRight: '1rem' }}
                  alt="Photograph of the Midnight MoonCat plushie; a blue, tortie-patterned, smiling MoonCat"
                />
              </picture>
              <picture>
                <img
                  src="/img/plush2.webp"
                  style={{ maxHeight: 200, maxWidth: '40%' }}
                  alt="Photograph of the Midnight MoonCat plushie; a blue, tortie-patterned, smiling MoonCat"
                />
              </picture>
            </p>
            <p>
              <a href="https://merch.mooncatrescue.com/" className="btn">
                Buy Now
              </a>
            </p>
          </section>
          <p>
            The first design is a <strong>glow-in-the-dark</strong> version of MidnightLightning&rsquo;s{' '}
            <Link href="/mooncats/1289">avatar MoonCat</Link>. Order now, to get in on the first batch produced, which will be shipping in January 2024.
          </p>
        </section>
      </section>
      <div className="text-container">
        <RandomMoonCatRow />
        <h1>Play With MoonCats!</h1>
        <section className="card">
          <p>Have a MoonCat of your own? Have some fun with them around ChainStation Alpha!</p>
          <ul>
            <li>
              Use them as your avatar on{' '}
              <a href="https://webb.game/" target="_blank" rel="noreferrer">
                WorldWideWebb
              </a>{' '}
              and see them walk around the virtual world with you.
            </li>
            <li>
              Let them wander around your{' '}
              <a href="https://isotile.com/" target="_blank" rel="noreferrer">
                Isotile
              </a>{' '}
              room to greet your guests.
            </li>
            <li>
              Dress them up for some fun at{' '}
              <a href="https://boutique.mooncat.community/" target="_blank" rel="noreferrer">
                the Boutique
              </a>{' '}
              to show off your personal style.
            </li>
            <li>
              Learn more about the project at the{' '}
              <a href="https://mooncat.community/about">MoonCat{ZWS}Community site</a>.
            </li>
          </ul>
        </section>
      </div>
    </div>
  )
}
export default Home
