import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'

const ZWS = '\u200B'
const pageTitle = 'Boutique'
const Boutique: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta name="description" property="og:description" content="Customization options for MoonCats" />
      </Head>
      <div className="text-container">
        <h1 className="hero">Boutique</h1>
        <section className="card">
          <p>
            <strong>Hard Hats Required!</strong> This area of ChainStation Alpha is undergoing some intense
            construction. So, please pardon the dust, and check back a bit later to see what pops up here!
          </p>
        </section>
        <section className="card-notice">
          <p>
            While this site is under construction, the Boutique can be accessed{' '}
            <a href="https://boutique.mooncat.community/">on the MoonCat{ZWS}Community website</a>.
          </p>
        </section>
      </div>
    </div>
  )
}
export default Boutique
