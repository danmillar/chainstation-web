import React, { useEffect, useState } from 'react'
import type { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Icon from 'components/Icon'
import LoadingIndicator from 'components/LoadingIndicator'
import MoonCatGrid from 'components/MoonCatSelectorGrid'
import { parseAbi } from 'viem'
import { useAccount, useContractRead } from 'wagmi'
import { useWeb3Modal } from '@web3modal/react'
import { customEvent } from 'lib/analytics'
import { Moment, MoonCatData } from 'lib/types'
import { MOMENTS_ADDRESS, switchToChain, useIsMounted } from 'lib/util'
import useTxStatus from 'lib/useTxStatus'
import WarningIndicator from 'components/WarningIndicator'
const moments: Moment[] = require('lib/moments_meta.json')

const ZWS = '\u200B'

const MOMENTS = {
  address: MOMENTS_ADDRESS as `0x${string}`,
  abi: parseAbi([
    'function isClaimable(uint256 momentId, uint256[] rescueOrders) external view returns (bool[])',
    'function batchClaim(uint256 momentId, uint256[] rescueOrders) external',
  ]),
} as const

interface MomentRef {
  tokenId: number
  moment: Moment
}

interface Props {
  moment: Moment
}

const MoonCatMomentClaim: NextPage<Props> = ({ moment }) => {
  const moonCats = moments[moment.momentId].moonCats
  const isMounted = useIsMounted()
  const { isConnected } = useAccount()
  const { open } = useWeb3Modal()
  const [claimableMoonCats, setClaimableMoonCats] = useState<number[]>([])
  const [selectedMoonCats, setSelectedMoonCats] = useState<Set<bigint>>(new Set())
  const { viewMessage, setStatus, processTransaction } = useTxStatus()

  const {
    data,
    status: claimableStatus,
    refetch,
  } = useContractRead({
    ...MOMENTS,
    functionName: 'isClaimable',
    args: [BigInt(moment.momentId), moonCats.map((m) => BigInt(m))],
    chainId: 1,
  })
  console.debug('contract data', data, claimableStatus)
  useEffect(() => {
    if (typeof data == 'undefined') return
    // Match MoonCat rescue order to their status
    let claimable = []
    for (let i = 0; i < moonCats.length; i++) {
      if (data[i]) {
        claimable.push(moonCats[i])
      }
    }
    setClaimableMoonCats(claimable)
  }, [moonCats, data])

  async function handleClaim() {
    if (selectedMoonCats.size == 0) {
      setStatus('error', 'No MoonCats selected')
      return
    }
    setStatus('building')
    if (!(await switchToChain(1))) {
      setStatus('error', 'Wrong network chain')
      return
    }

    let rs = await processTransaction({
      ...MOMENTS,
      functionName: 'batchClaim',
      args: [BigInt(moment.momentId), Array.from(selectedMoonCats)],
    })
    if (rs) {
      customEvent('moments_claim', {
        'mooncats': Array.from(selectedMoonCats).map((num) => Number(num)),
        'moment': moment.momentId,
      })
      setSelectedMoonCats(new Set())
      refetch()
    }
  }
  function handleClick(mc: MoonCatData) {
    const rescueOrder = BigInt(mc.rescueOrder)
    setSelectedMoonCats((current) => {
      let next = new Set(current)
      if (next.has(rescueOrder)) {
        next.delete(rescueOrder)
      } else {
        next.add(rescueOrder)
      }
      return next
    })
  }

  let bodyView: React.ReactNode
  if (!isMounted || claimableStatus == 'idle' || claimableStatus == 'loading') {
    bodyView = <LoadingIndicator />
  } else if (claimableMoonCats.length == 0) {
    bodyView = <WarningIndicator message="All tokens have been claimed for this Moment" />
  } else {
    const actionButton = isConnected ? (
      <button onClick={handleClaim}>Claim</button>
    ) : (
      <button onClick={() => open()}>Connect</button>
    )

    const label = selectedMoonCats.size == 1 ? 'MoonCat' : 'MoonCats'
    const selectionView =
      selectedMoonCats.size > 0 ? (
        <>
          <p>
            {selectedMoonCats.size} {label} selected for Claiming
          </p>
          <p>{actionButton}</p>
          {viewMessage}
        </>
      ) : (
        <p>Click to select some MoonCats to claim this Moment for</p>
      )

    bodyView = (
      <>
        <MoonCatGrid
          moonCats={claimableMoonCats}
          apiPage="mooncats"
          minCellWidth={100}
          highlightedMoonCats={Array.from(selectedMoonCats).map((i) => Number(i))}
          onClick={handleClick}
        >
          {(mc: MoonCatData) => <p>#{mc.rescueOrder}</p>}
        </MoonCatGrid>
        <div className="text-container" style={{ textAlign: 'center' }}>
          <section className="card">{selectionView}</section>
        </div>
      </>
    )
  }

  return (
    <div id="content-container">
      <Head>
        <title>{moment.meta.name + ' Claim'}</title>
        <meta property="og:title" content={moment.meta.name + ' Claim'} />
        <meta
          name="description"
          property="og:description"
          content="Claim a MoonCatMoment NFT for MoonCats who qualify."
        />
      </Head>
      <nav className="breadcrumb">
        <Link href={'/moments/' + moment.momentId}>
          <a>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Back to Moment details
          </a>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">{moment.meta.name} Claim</h1>
        <section className="card">
          <p>
            <strong>Claim your Moment!</strong> When a MoonCat{ZWS}Moment token is claimed, it gets given directly to
            the MoonCat who participated in that Moment. Anyone can pay the gas cost to claim a Moment for a MoonCat,
            but only the MoonCat&rsquo;s owner will be able to transfer the token out of the MoonCat&rsquo;s purrse
            after it&rsquo;s claimed.
          </p>
        </section>
      </div>
      {bodyView}
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const moments: Moment[] = require('lib/moments_meta.json')
  if (typeof ctx.params == 'undefined') {
    console.log('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  const moment = moments.find((m) => m.momentId == Number(id))
  if (typeof moment == 'undefined') {
    console.error('No metadata for Moment', id)
    return { notFound: true }
  }

  return { props: { moment } }
}

export default MoonCatMomentClaim
