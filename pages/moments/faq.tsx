import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Icon from 'components/Icon'

const ZWS = '\u200B'
const pageTitle = 'MoonCatMoments - FAQ'
const MoonCatMoments: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Snapshots of important moments in the MoonCats lore!"
        />
      </Head>
      <nav className="breadcrumb">
        <Link href="/moments">
          <a>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all Moments
          </a>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Moments FAQ</h1>
        <section className="card-help">
          <h3>What are MoonCat{ZWS}Moments?</h3>
          <p>
            An ERC721-compatible NFT collection that represents special moments/achievements in the MoonCat Ecosystem.
          </p>

          <h3>How do I participate in an upcoming Moment?</h3>
          <p>
            Each Moment is different for what the criteria are for participating. Keep an eye on the{' '}
            <a href="https://twitter.com/mooncatrescue" rel="noreferrer" target="_blank">
              MoonCatRescue social feed
            </a>{' '}
            and announcements in the{' '}
            <a href="discord.gg/mooncats" rel="noreferrer" target="_blank">
              Community Discord
            </a>{' '}
            to know when a new Moment is being organized and how to participate.
          </p>

          <h3>My MoonCat&rsquo;s in the picture! Where&rsquo;s my token?</h3>
          <p>
            New MoonCat{ZWS}Moment are delivered directly to the MoonCats that are part of the Moment. This is done
            using Acclimated MoonCats&rsquo; ERC998 capability; in most wallet software they&rsquo;ll show up not as
            owned by your wallet, but here on the ChainStation they&rsquo;ll show up in your profile.
          </p>
          <p>
            If you wish you can transfer Moments out of your MoonCat&rsquo;s posession to your own wallet if
            you&rsquo;ve signed into the ChainStation.
          </p>

          <h3>I missed the boat! Can I still participate?</h3>
          <p>
            MoonCat{ZWS}Moments are transferrable, so if you find someone who owns a Moment that you want, offering a
            trade or using NFT marketplaces is a way to acquire Moments that have already happened
          </p>

          <h3>What does a Moment do?</h3>
          <p>
            MoonCat{ZWS}Moments are primarily mementos; a badge of honor showing your participating in past events and
            tenure in the MoonCatRescue ecosystem. Different Moments may be used for random perks like raffles or
            events, but the primary functionality of owning a Moment is it unlocks the rights to a special animated
            version of the Moment, as well as a higher-resolution version of the Moment image that&rsquo;s in the public
            metadata.
          </p>
          <p>
            To get access to those additional perks from the Moments you own, check out{' '}
            <Link href="/profile">your profile page</Link> and look under the &ldquo;Treasures&rdquo; listing.
          </p>
        </section>
      </div>
    </div>
  )
}
export default MoonCatMoments
